Source: dnssecjava
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Ingo Bauersachs <ingo@jitsi.org>
Section: java
Priority: optional
Build-Depends: debhelper-compat (= 13),
               default-jdk,
               default-jdk-doc,
               junit4,
               libbcprov-java,
               libdnsjava-java,
               libjoda-time-java,
               liblog4j1.2-java,
               libmaven-bundle-plugin-java,
               libmaven-javadoc-plugin-java,
               libslf4j-java,
               maven-debian-helper
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/java-team/dnssecjava
Vcs-Git: https://salsa.debian.org/java-team/dnssecjava.git
Homepage: https://github.com/ibauersachs/dnssecjava

Package: libdnssecjava-java
Architecture: all
Depends: ${maven:Depends},
         ${misc:Depends}
Suggests: libdnssecjava-java-doc,
          ${maven:OptionalDepends}
Description: DNSSEC validating stub resolver for Java
 dnssecjava is an implementation of a DNSSEC validating stub
 resolver in Java. It is meant to be used as a resolver when
 using the dnsjava library.

Package: libdnssecjava-java-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Recommends: ${maven:DocDepends},
            ${maven:DocOptionalDepends}
Suggests: libdnssecjava-java
Description: DNSSEC validating stub resolver for Java (documentation)
 dnssecjava is an implementation of a DNSSEC validating stub
 resolver in Java. It is meant to be used as a resolver when
 using the dnsjava library.
 .
 This package contains the API documentation of libdnssecjava-java.
